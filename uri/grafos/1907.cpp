#include <iostream>
#include <cstring>
#include <queue>
#include <cstdio>

using namespace std;

#define MAX 1046
#define VISITED 1
#define UNVISITED 0
#define ii pair<int,int>
int c;
char x[MAX][MAX];

void check_adj(queue<ii> &q, int k, int y) {
	if(x[k][y] != '.') return;
	x[k][y] = 'o';
	q.push(ii(k,y));
}
void bfs(ii start){
    queue<ii> q;
    q.push(start);

    while(!q.empty()){
		ii item = q.front(); q.pop();
		x[item.first][item.second] = 'o';
		check_adj(q, item.first+1, item.second);
		check_adj(q, item.first, item.second+1);
		check_adj(q, item.first-1,item.second);
		check_adj(q, item.first,item.second-1);
    }
}

int main(){

	int m,n;
	int i, k;
	memset(x,'x', sizeof(x));
	scanf("%d %d", &m, &n);
	char trash;
	c=0;
	scanf("%c",&trash);
	for(i=1; i<=m; ++i) {
		for(k=1; k<=n; ++k) {
			scanf("%c", &x[i][k]);

		}
			scanf("%c",&trash);
	}


	for(i=1; i<=m; ++i) {
		for(k=1; k<=n; ++k) {
			if(x[i][k] == '.') {
				c++;
				bfs(ii(i,k));
			}
		}
	}

	cout << c << endl;

  return 0;
}
