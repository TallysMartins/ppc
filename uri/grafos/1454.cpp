#include <iostream>
#include <queue>
#include <cstring>
#include <vector>
#define MAX 115
#define INF 2000000

using namespace std;
using ii=pair<int,int>;

vector<ii>adj[MAX];
unsigned int dist[MAX];
int visited[MAX];
using ii = pair<int,int>;
using edge = pair<int, ii>;
pair<int,int>parents[MAX];

class UFDS {
private:
  vector<int> p, rank, setSizes;// p = vector que guarda os pais da galera, 
  int numSets;	// rank guarda o rank de cada vertice

public:
  UFDS(int N) {
    numSets = N;
    rank.assign(N, 0);
    p.assign(N, 0);
    for (int i = 0; i < N; i++)
      p[i] = i;
    setSizes.assign(N, 1);
  }
  int findSet(int i) {
    return (p[i] == i) ? i : p[i] = findSet(p[i]);
  }
  bool isSameSet(int i, int j) {
    return findSet(i) == findSet(j);
  }
  void unionSet(int i, int j) {
    if (!isSameSet(i, j)) {
      int x = findSet(i), y = findSet(j);

      if (rank[x] > rank[y]) {
        setSizes[findSet(x)] += setSizes[findSet(y)];
        p[y] = x;
      } else {
        setSizes[findSet(y)] += setSizes[findSet(x)];
        p[x] = y;
        if (rank[x] == rank[y])
          rank[y]++;
      }
      numSets--;
    }
  }
  int setSize(int i) {
    return setSizes[findSet(i)];
  }
  int numDisjointSets() {
    return numSets;
  }
};


void dijkstra(int s, int d, int n){
	memset(dist,-1, sizeof dist);
	memset(parents,0, sizeof parents);
	queue<int>pq;
	pq.push(s);
	dist[s] = 0;
	int maior = 0;

	int u,w,sum,deepth;
	while(not pq.empty()) {
		int item = pq.front(); pq.pop();
		if(item == d)
			break;
		for(auto it : adj[item]) {
			if(dist[it.first] > dist[item] + it.second) {
				dist[it.first] = dist[item] + it.second;
				pq.push(it.first);
				parents[it.first] = ii(item,it.second);
			}
		}
	}

	int no = d;
	while(no !=  s) {
		maior=max(maior,parents[no].second);
		no = parents[no].first;
	}
	cout << maior << endl;
}

void kruskal(int V, priority_queue<edge> &q) {
	UFDS ufds(V+1);
	int D = 0;
	memset(adj,0,sizeof adj);
	while(not q.empty()) {
		auto item = q.top();
		q.pop();
		int w = -item.first;
		int u  = item.second.first;
		int v = item.second.second;

		if(not ufds.isSameSet(u,v)) {
			ufds.unionSet(u,v);
			/* cout << "colocando as aresta" << w << "que une " << u << " a " << v <<endl; */
			adj[u].push_back(ii(v,w));
			adj[v].push_back(ii(u,w));
		}
	}

}

int main() {
	int c,v;
	int a,b,g;
	int q;
	int i = 1;
	while(scanf("%d %d", &c, &v), c|v) {
		priority_queue<edge> pq;
		while(v--){
			scanf("%d %d %d", &a, &b, &g);
			pq.push(edge(-g, ii(a,b)));
		}
		scanf("%d", &q);
		printf("Instancia %d\n", i);
		kruskal(c, pq);
		int xd =0;
		while(q--){
			scanf("%d %d", &a, &b);
			if(xd != a) {
				dijkstra(a,b,c);
				xd = a;
			}else {
				int maior =0;
				int no = b;
				while(no !=  a) {
					maior=max(maior,parents[no].second);
					no = parents[no].first;
				}
				cout << maior << endl;

			}
		}
		++i;
	}

	return 0;
}
