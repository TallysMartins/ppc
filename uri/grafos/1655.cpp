#include <iostream>
#include <cstring>

using namespace std;

#define MAX 120
const int oo = 0;
double grid[MAX][MAX];

int main() {
  int n, m;
  int a,b;
  double p;

  while(scanf("%d %d", &n, &m), n) {
    memset(grid, 0, sizeof grid);
    for(int i=1; i<=m; ++i) {
      scanf("%d %d %lf", &a, &b, &p);
      grid[a][b] = p*0.01;
      grid[b][a] = p*0.01;
    }

    for(int k = 1 ; k <=n ; ++ k)
       for(int i = 1 ; i <=n ; ++ i)
          for(int j = 1 ; j <=n ; ++ j)
            grid[i][j] = max(grid[i][j], grid[i][k] * grid[k][j]);
          
    printf("%.6lf percent\n", grid[1][n]*100);
  }


  return 0;

}
