#include <iostream>
#include <queue>
#include <cstring>
#include <vector>
#define MAX 10005
#define INF 2000000

using namespace std;
using ii=pair<int,int>;

vector<ii>adj[MAX];
int dist[MAX][2];
int visited[MAX];

int impar(int n) {
	return n&1;
}

void dijkstra(int s, int d, int n){
	for(int i=0; i<=n+1; ++i){
		dist[i][0] = INF;
		dist[i][1] = INF;
	}
	queue<ii>pq;
	pq.push(ii(s,0));
	dist[s][0] = 0;

	int u,w,sum,deepth;
	while(not pq.empty()) {
		auto item = pq.front(); pq.pop();
		u = item.first;
		deepth = item.second;
		int odd = deepth%2;

		for(auto it : adj[u]) {
			if(dist[it.first][(odd+1)%2] > dist[u][odd] + it.second) {
				dist[it.first][(odd+1)%2] = dist[u][odd] + it.second;
				pq.push(ii(it.first,deepth+1));
			}
		}
	}

	if(dist[d][0] == INF) {
		printf("-1\n");
	}else {
		printf("%d\n",dist[d][0]);
	}
}

int main() {
	memset(adj,0,sizeof adj);
	memset(visited,0,sizeof visited);
	int c,v;
	scanf("%d %d", &c, &v);

	int a,b,g;
	while(v--){
		scanf("%d %d %d", &a, &b, &g);
		adj[a].push_back(ii(b,g));
		adj[b].push_back(ii(a,g));
	}

	dijkstra(1,c,c);
	return 0;
}
