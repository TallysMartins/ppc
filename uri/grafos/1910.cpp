#include <cstring>
#include <queue>
#include <cstdio>
#include <algorithm>
#include <cstdio>

using namespace std;
bool sync_with_stdio(bool sync = false);
#define lli int
#define ii int
#define MAX 100009
#define VISITED 1
#define UNVISITED 0

unsigned int visited[MAX];
lli forbidden[MAX];
int c;
lli O, D, K;

void check_channel(queue<ii> &q, int item, int new_item) {
	if(new_item <= 0 || new_item > 100000 || forbidden[new_item]) return;
	if(visited[new_item] != -1) return;
	visited[new_item] = visited[item] + 1;
	q.push(new_item);
}

void bfs(lli unvisited_element){
    queue<ii> myq;
    myq.push(unvisited_element);
	visited[unvisited_element] = 0;
    while(!myq.empty()){
		ii item = myq.front(); myq.pop();
		c=visited[item];
		if(item == D) return;
		check_channel(myq, item, item + 1);
		check_channel(myq, item, item - 1);
		check_channel(myq, item, item * 2);
		check_channel(myq, item, item * 3);
		if(not (item & 1)) {
			check_channel(myq, item, item / 2);
		}
    }

	c = -1;
}

int main(){
	lli forbidden_channel;
	while(scanf("%d %d %d", &O, &D, &K), O, D) {
		memset(forbidden, 0, sizeof(forbidden));
		memset(visited, -1, sizeof(visited));
		c = 0;
		while(K--) {
			scanf("%d", &forbidden_channel);
			forbidden[forbidden_channel] = 1;
		}
		bfs(O);
		printf("%d\n", c);
	}

	return 0;
}
