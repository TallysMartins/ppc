/*  Problema das baleias
	algoritmo que detecta pontos de articulacao
    conta se ha ponto de articulaçao entao nao da pra dividir as baleias */
#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;

#define MAX 1001
#define MAXL 4096

#define UNVISITED  0

int adj[MAX][MAX];
int dfs_num[MAX], dfs_low[MAX], parents[MAX], articulations[MAX];
int num_counter, root_children;
char line[MAXL];
bool bazinga;
int visited[MAX];

void dfs(int u, int n)
{
    dfs_low[u] = dfs_num[u] = ++num_counter;
	visited[u] = 1;

    for (int i = 1; i <= n; ++i) {
		if(adj[u][i] ) {
			int v = i;

			if (not visited[v])
			{
				parents[v] = u;

				if (u == 1) ++root_children;

				dfs(v, n);

				if (dfs_low[v] >= dfs_num[u]){
					articulations[u] = 1;
					if(u != 1)
						bazinga = false;
				}

				dfs_low[u] = min(dfs_low[u], dfs_low[v]);
			} else if (v != parents[u])
				dfs_low[u] = min(dfs_low[u], dfs_num[v]);
		}
	}
}

int main()
{
    while (fgets(line, MAXL, stdin))
    {
        int N = atoi(line);
		int nu = N;

		int k = 1;
		int i = 1;
		int v;

        while (N--)
        {
            fgets(line, MAXL, stdin);
            istringstream is(line);

			i = 1;
            while (is >> v)
            {
                adj[k][i] = v;
				++i;
            }
			++k;
        }

        memset(dfs_num, UNVISITED, sizeof dfs_num);
        memset(dfs_low, UNVISITED, sizeof dfs_low);
        memset(parents, 0, sizeof parents);
        memset(articulations, 0, sizeof articulations);
        memset(visited, 0, sizeof visited);
        num_counter = root_children = 0;

		bazinga = true;
        dfs(1, nu);

        articulations[1] = (root_children > 1 ? 1 : 0);
		parents[1] = 1;

        int points = 0;

        /* for (int i = 0; i < nu; ++i) { */
        /*     points += articulations[i]; */
		/* } */

		if(bazinga)
			cout << "Bazinga!" << endl;
		else
			cout << "Fail!" << endl;
    }

    return 0;
}
