#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>
#include <unistd.h>
using namespace std;


#define MAX 1001
#define MAXL 4096

#define UNVISITED  0

int cores[MAX];
bool bicorolable;
vector<int>graph[MAX];
char line[MAXL];
int visited[MAX];

int cor_inversa(int color){
	return	1 - color;// se chega 0 -> 1-0 =  1;
   			  //se chega 1 -> 1 -1 = 0
}

void dfs(int v, int color){
	if(not bicorolable)
		return;
	cores[v] = color;
	for(auto it : graph[v]) {
		if(cores[it] == -1) {
			dfs(it,cor_inversa(color));
		}else if(cores[it] == color) {
			bicorolable = false;
			return;
		}
	}
}


int main()
{
	int N;
	int v;
	while(fgets(line,MAXL,stdin), atoi(line)) {
		int nu = atoi(line);
		memset(cores, -1, sizeof(cores));
		memset(graph,0,sizeof(graph));
		int vertice;
		while(nu--) {
			fgets(line,MAXL,stdin);

			vertice = atoi(line);
			fgets(line, MAXL,stdin);
			istringstream is(line);
			while( is >> v) {
				graph[vertice].push_back(v);
			}
		}

		bicorolable = true;
		dfs(1,0);
		if(bicorolable)
			cout << "SIM" << endl;
		else
			cout << "NAO" << endl;
	}

    return 0;
}
