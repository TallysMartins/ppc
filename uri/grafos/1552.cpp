/* algoritmo de kruskall para resolver o problema do subgrafo conectado */
/* de menor custo */
/* responde a pergunta: quais arestas eu removo e mantenho o grafo conectado com */
/* as arestas de menor custo? */
#include <cstdio>
#include <queue>
#include <cstring>
#include <cmath>
#define MAX 510
using namespace std;
using ii = pair<double,double>;
using edge = pair<double , ii>;

ii pontos[MAX];

class UFDS {
private:
  vector<int> p, rank, setSizes;// p = vector que guarda os pais da galera, 
  int numSets;	// rank guarda o rank de cada vertice

public:
  UFDS(int N) {
    numSets = N;
    rank.assign(N, 0);
    p.assign(N, 0);
    for (int i = 0; i < N; i++)
      p[i] = i;
    /* setSizes.assign(N, 1); */
  }
  int findSet(int i) {
    return (p[i] == i) ? i : p[i] = findSet(p[i]);
  }
  bool isSameSet(int i, int j) {
    return findSet(i) == findSet(j);
  }
  void unionSet(int i, int j) {
    if (!isSameSet(i, j)) {
      int x = findSet(i), y = findSet(j);

      if (rank[x] > rank[y]) {
        /* setSizes[findSet(x)] += setSizes[findSet(y)]; */
        p[y] = x;
      } else {
        /* setSizes[findSet(y)] += setSizes[findSet(x)]; */
        p[x] = y;
        if (rank[x] == rank[y])
          rank[y]++;
      }
      /* numSets--; */
    }
  }
  int setSize(int i) {
    return setSizes[findSet(i)];
  }
  int numDisjointSets() {
    return numSets;
  }
};

inline double D(int x1, int y1, int x2, int y2){
   return sqrt((double)(((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))));
}

double kruskal(int V, priority_queue<edge> &q) {
	UFDS ufds(V+1);
	double D = 0;
	while(not q.empty()) {
		auto item = q.top();
		q.pop();
		double w = -item.first;
		int u  = item.second.first;
		int v = item.second.second;

		if(not ufds.isSameSet(u,v)) {
			ufds.unionSet(u,v);
			D += w;
		}
	}

	return D;

}
int main() {

	int T;
	int  n;

	priority_queue<edge> pq;
	double x,y,z;

	scanf("%d", &T);
	while(T--) {
		scanf("%d", &n);
		memset(pontos,0,sizeof pontos);

		for(int i=1; i<=n; ++i) {
			scanf("%lf %lf", &x, &y);
			pontos[i] = ii(x,y);

			for(int k=1; k<=i; ++k) {
					z = hypot(pontos[k].first-x, pontos[k].second-y);
					pq.push(edge(-z,ii(i,k)));
			}
		}
		printf("%.2f\n", kruskal(n,pq)/100);
	}

	return 0;
}
