#include <iostream>
#include <cstring>
#include <queue>
#include <cstdio>
#include <algorithm>
#include <cstdio>
#include <map>
#include <cstdlib>
#include <unistd.h>

using namespace std;

#define ii pair<int,int>
typedef struct g{
	pair <int, int>a;
	pair <int, int>b;
	pair <int, int>c;
	int deepth;
	char grid[11][11];
} g;

ii a,b,c;
ii p1,p2,p3;
char grid[11][11];
int vis[102][102][102];
int order;
int cont;
map<int,int>v;

bool operator!=(const g& lhs, const g& rhs)
{
	lhs.a != rhs.a || lhs.b != rhs.b || lhs.c != rhs.c;
}

void visit(g &item) {
	int sq_a = (item.a.first-1)*order+item.a.second;
	int sq_b = (item.b.first-1)*order+item.b.second;
	int sq_c = (item.c.first-1)*order+item.c.second;
	vector<int> a = { sq_a, sq_b, sq_c};
	sort(a.begin(), a.end());
	vis[a[0]][a[1]][a[2]]= 1;
}


int visited(g &item) {
	int sq_a = (item.a.first-1)*order+item.a.second;
	int sq_b = (item.b.first-1)*order+item.b.second;
	int sq_c = (item.c.first-1)*order+item.c.second;
	vector<int> a = { sq_a, sq_b, sq_c};
	sort(a.begin(), a.end());
	return (vis[a[0]][a[1]][a[2]]== 1);
}

void make_move(char id, ii &cord, int x,int y) {
	int xf = cord.first +x;
	int yf = cord.second +y;
	if(grid[xf][yf] == '.' || grid[xf][yf] == 'X') {
		/* grid[cord.first][cord.second] = '.'; */
		cord.first = xf;
		cord.second = yf;
	}else if(grid[xf][yf] == 'R') {
		if(grid[xf+x][yf+y] == '.') {
			/* grid[cord.first][cord.second] = '.'; */
			cord.first = xf;
			cord.second = yf;
		}else if(grid[xf+x][yf+y] == 'R') {
			if(grid[xf+x*2][yf+y*2] == '.') {
				/* grid[cord.first][cord.second] = '.'; */
				cord.first = xf;
				cord.second = yf;
			}
		}
	}

}

void fix_grid(g &old_pos, g &new_pos) {
	grid[p1.first][p1.second] = 'X';
	grid[p2.first][p2.second] = 'X';
	grid[p3.first][p3.second] = 'X';
	grid[old_pos.a.first][old_pos.a.second] = '.';
	grid[old_pos.b.first][old_pos.b.second] = '.';
	grid[old_pos.c.first][old_pos.c.second] = '.';
	grid[new_pos.a.first][new_pos.a.second] = 'R';
	grid[new_pos.b.first][new_pos.b.second] = 'R';
	grid[new_pos.c.first][new_pos.c.second] = 'R';
}

g move(char d, g item) {
	switch(d){
		case 'D': {
					make_move('R',item.a, 1, 0);
					make_move('R',item.b, 1, 0);
					make_move('R',item.c, 1, 0);
					break;
				  }


		case 'R': {
					make_move('R',item.a, 0, 1);
					make_move('R',item.b, 0, 1);
					make_move('R',item.c, 0, 1);
					break;
				  }

		case 'U': {
					make_move('R',item.a, -1, 0);
					make_move('R',item.b, -1, 0);
					make_move('R',item.c, -1, 0);
					break;
				  }

		case 'L': {
					make_move('R',item.a, 0, -1);
					make_move('R',item.b, 0, -1);
					make_move('R',item.c, 0, -1);
					break;
				  }
	}
	return item;
}

int arrived(g &item) {
	bool arr = false;
	if(grid[p1.first][p1.second] == 'R' && grid[p2.first][p2.second] == 'R' && grid[p3.first][p3.second] == 'R') {
		arr = true;
	}
	return arr;
}

void bfs(g rob, int deep){
	/* for(int i=0; i<=order+1; ++i) { */
	/* 	for(int k=0; k<=order+1; ++k) { */
	/* 		cout << grid[i][k]; */
	/* 	} */
	/* 	cout << endl; */
	/* } */
	/* sleep(1.5); */
	if(arrived(rob)) {
		if(deep < cont)
			cont = deep;
		return;
	}
	visit(rob);
	g aux_r = move('R', rob);
	g aux_u = move('U', rob);
	g aux_l = move('L', rob);
	g aux_d = move('D', rob);
	if(!visited(aux_r) && aux_r != rob){
		fix_grid(rob, aux_r);
		deep++;
		bfs(aux_r, deep);
		fix_grid(rob, aux_r);
		deep--;
	}
	if(!visited(aux_u) && aux_u != rob){
		fix_grid(rob, aux_u);
		deep++;
		bfs(aux_u, deep);
		fix_grid(rob, aux_u);
		deep--;
	}
	if(!visited(aux_l) && aux_l != rob){
		fix_grid(rob, aux_l);
		deep++;
		bfs(aux_l, deep);
		fix_grid(rob, aux_l);
		deep--;
	}
	if(!visited( aux_d) && aux_d != rob){
		fix_grid(rob, aux_d);
		deep++;
		bfs(aux_d, deep);
		fix_grid(rob, aux_d);
		deep--;
	}
	deep--;
}

int main(){
	int T;
	cin >> T;
	char tmp;
	for(int j=1; j<=T; ++j) {
		memset(grid, '0', sizeof(grid));
		memset(vis, 0, sizeof(vis));
		cont = 99999;
		ii x[3];
		int n = 0;
		cin >> order;
		for(int i=1; i<=order; ++i) {
			for(int k=1; k<=order; ++k) {
				cin >> grid[i][k];
				if(grid[i][k] == 'X') {
					x[n] = ii(i,k);
					++n;
				}
				if(grid[i][k] == 'A'){
					a = ii(i,k);
					grid[i][k] = 'R';
				}
				if(grid[i][k] == 'B') {
					b = ii(i,k);
					grid[i][k] = 'R';
				}
				if(grid[i][k] == 'C'){
					c = ii(i,k);
					grid[i][k] = 'R';
				}
			}
			scanf("%c", &tmp);
		}
		p1 = x[0]; p2 = x[1]; p3 = x[2];
		g group;
		group.a = a;
		group.b = b;
		group.c = c;
		group.deepth = 0;
		memcpy(group.grid, grid, sizeof(grid));
		bfs(group,0);
		if (cont == 99999) {
			cout << "Case " << j << ": " <<  "trapped" << endl;
		}else {
			cout << "Case " << j << ": " << cont << endl;
		}

	}

	return 0;
}
