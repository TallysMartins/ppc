//WA 30%
//triste
#include <iostream>
#include <cstring>
#include <queue>
#include <stack>

#define MAX 2005

using namespace std;
using vi=vector<int>;
using si=stack<int>;

vi adj[MAX];

#define UNVISITED  0

int dfs_num[MAX], dfs_low[MAX], parents[MAX], articulations[MAX];
int num_counter, root_children;
int visited[MAX];

dsajkdsaj
void dfs(int u)
{
	dfs_low[u] = dfs_num[u] = ++num_counter;

	for (auto it: adj[u])
	{
		int v = it;

		if (dfs_num[v] == UNVISITED)
		{
			parents[v] = u;

			if (u == 1) ++root_children;

			dfs(v);

			if (dfs_low[v] >= dfs_num[u])
				articulations[u] = 1;

			dfs_low[u] = min(dfs_low[u], dfs_low[v]);
		} else if (v != parents[u])
			dfs_low[u] = min(dfs_low[u], dfs_num[v]);
	}
}


int main() {

	int n, m;
	int v,w,p;
	int v1,w1,p1;
	while(scanf("%d %d", &n, &m) != EOF) {
		memset(adj,0, sizeof adj);
		memset(dfs_num, UNVISITED, sizeof dfs_num);
		memset(dfs_low, UNVISITED, sizeof dfs_low);
		memset(parents, 0, sizeof parents);
		memset(articulations, 0, sizeof articulations);
		memset(visited, 0, sizeof visited);
		scanf("%d %d %d", &v1, &w1, &p1);
		m--;

		while(m--) {
			scanf("%d %d %d", &v, &w, &p);
			adj[v].push_back(w);
			if(p&2)
				adj[w].push_back(v);
		}

		num_counter = root_children = 0;
		dfs(1);
		int points = 0;
		int conexo = 1;

		for (int i = 1; i <= n; ++i) {
			if(dfs_num[i] == 0)
				conexo = 0;
		}
		adj[v1].push_back(w1);
		if(p1&2)
			adj[w1].push_back(v1);

		memset(dfs_num, UNVISITED, sizeof dfs_num);
		memset(dfs_low, UNVISITED, sizeof dfs_low);
		num_counter = root_children = 0;
		articulations[1] = (root_children > 1 ? 1 : 0);

		dfs(1);
		int impossivel = 0;
		if(dfs_num[v1] < dfs_low[w1])
			impossivel = 1;

		points = articulations[1];
		for (int i = 1; i <= n; ++i) {
			for(auto it : adj[i]){
				if(dfs_num[i] < dfs_low[it]) {
					points++;
				}
			}
		}


		if(conexo)
			cout << "-" << endl;
		else if(impossivel)
			printf("*\n");
		else if(points > 1)
			cout << "1"<< endl;
		else
			cout << "2" << endl;
	}


	return 0;
}


