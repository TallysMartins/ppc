
#include <iostream>
#include <queue>
#include <cstring>
#include <vector>
#define MAX 510
#define INF 2000000

using namespace std;
using ii=pair<int,int>;

unsigned int dist[MAX][MAX];
char matrix[MAX][MAX];

void check_adj(queue<ii> &q, ii &item, int a, int b) {
	if(matrix[a][b] == '0' || matrix[a][b] == '#') {
		return;
	}
	int ux = item.first;
	int uy = item.second;
	int w;
	if(matrix[a][b] == '.' || matrix[a][b] == 'H' || matrix[a][b] == 'E') {
		w = 0;
	}else {
		w = matrix[a][b] - '0';
	}

	if(dist[a][b] > dist[ux][uy] + w) {
		dist[a][b] = dist[ux][uy] +w;
		q.push(ii(a,b));
	}
}

void dijkstra(int x, int y, int c, int v){
	for(int i=0; i<=c+1; ++i) {
		for(int j=0; j<=v+1; ++j) {
			dist[i][j] = INF;
		}
	}
	dist[x][y] = 0;
	queue<ii>pq;
	pq.push(ii(x,y));

	int ux, uy, w;
	while(not pq.empty()) {
		auto item = pq.front(); pq.pop();
		ux = item.first;
		uy = item.second;
		check_adj(pq,item,ux,uy+1);
		check_adj(pq,item,ux-1,uy);
		check_adj(pq,item,ux,uy-1);
		check_adj(pq,item,ux+1,uy);
	}

}

int main() {
	memset(matrix,48,sizeof matrix);
	int c,v;
	scanf("%d %d", &c, &v);

	int a,b,g;
	int hercules[2];
	int euterpe[2];
	for(int i=1; i<=c; ++i) {
		for(int j=1; j<=v; ++j) {
			scanf(" %c ", &matrix[i][j]);
			if(matrix[i][j] == 'H') {
				hercules[0] = i;
				hercules[1] = j;
			}else if(matrix[i][j] == 'E') {
				euterpe[0] = i;
				euterpe[1] = j;
			}
		}
	}

	dijkstra(hercules[0], hercules[1], c, v);

	if(dist[euterpe[0]][euterpe[1]] >= INF) {
		printf("ARTSKJID\n");
	}else {
		printf("%d\n",dist[euterpe[0]][euterpe[1]]);
	}
	return 0;
}
