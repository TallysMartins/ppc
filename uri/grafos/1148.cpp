#include <iostream>
#include <vector>
#include <cstring>
#include <queue>

using namespace std;
#define MAX 520
#define INF 2000000000
using ii=pair<int,int>;

vector<int>adj[MAX];
int matrix[MAX][MAX];
int dist[MAX];
int visited[MAX];

void ball_ford(int N, int s, int d) {
	for(int i=0; i<=N; ++i){
			dist[i] = INF;
	}

	dist[s] = 0;
	bool att=false;
	for(int j=1; j<N; ++j) {
		for(int i=1; i<=N; ++i) {
			for(auto it : adj[i]) {
				int res = dist[i] + matrix[i][it];
				if(dist[it] > res) {
					dist[it] = res;
				}
			}
		}
	}

	dist[d] == INF ? ( cout << "Nao e possivel entregar a carta") : (cout << dist[d]);
	cout << endl;

}
int main() {

	int N, E;
	int u, v, h;
	int q;
	int T = 0;
	while(scanf("%d %d", &N, &E), N || E){
		if(T)
			cout << endl;
		++T;

		for(int i=0; i<=N; ++i){
			adj[i].clear();
			for(int k=0; k<=N; ++k)
				matrix[i][k] = INF;
		}

		for(int i=0; i < E; ++i) {
			scanf("%d %d %d", &u, &v, &h);
			adj[u].push_back(v);
			matrix[u][v] = h;

			if(matrix[v][u] < INF) {
				matrix[u][v] = 0;
				matrix[v][u] = 0;
			}
		}
		scanf("%d", &q);

		int source, destiny;
		int v = 0;
		for(int i=0; i < q; ++i){
			scanf("%d %d", &source, &destiny);
			if(source == v) {
				dist[destiny] == INF ? ( cout << "Nao e possivel entregar a carta") : (cout << dist[destiny]);
				cout << endl;
			}else {
				v = source;
				ball_ford(N, source, destiny);
			}
		}
		N = 0;
		E = 0;
		q = 0;
	}

	return 0;


}
