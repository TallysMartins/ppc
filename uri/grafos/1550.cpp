#include <iostream>
#include <cstring>
#include <queue>
#include <cstdio>
#include <algorithm>
#include <cstdio>

using namespace std;

#define ii pair<int,int>
#define MAX 10000
#define VISITED 1
#define UNVISITED 0
ii adj[MAX];
unsigned int visited[MAX];
int c;
int a,b;

int reversed(int number) {
	int reversed = 0;
	while(number!= 0) {
		reversed = reversed * 10;
		reversed = reversed + number%10;
		number = number/10;
	}
	return reversed;
}

void roll(){
    for(int i=0; i<=10000; ++i){
        adj[i].first = i+1;
        adj[i].second = reversed(i);
    }
}

void bfs(int unvisited_element){
    queue<ii> myq;
    myq.push(ii(unvisited_element,0));

    while(!myq.empty()){
		ii item = myq.front(); myq.pop();
		int bt1 = adj[item.first].first;
		int bt2 = adj[item.first].second;
		visited[item.first] = VISITED;

		if(bt1 == b || bt2  == b){
			c=item.second+1;
			return;
		}else {
			if(visited[bt1] == 0 && bt1 < 10000)
				myq.push(ii(bt1, item.second+1));
			if(visited[bt2] == 0 && bt2 < 10000)
				myq.push(ii(bt2, item.second+1));
			c=item.second+1;
		}
    }
}

int main(){
	int T;
	scanf("%d", &T);
	roll();
	for(int i=0; i<T; ++i) {
		memset(visited, 0, sizeof(visited));
		c = 0;
		scanf("%d %d", &a, &b);
		bfs(a);
		cout << c <<  endl;
	}

	return 0;
}
