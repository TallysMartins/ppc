#include <iostream>
#include <cstring>
#include <queue>
#include <stack>

#define MAX 2005

using namespace std;
using vi=vector<int>;
using si=stack<int>;

vi adj[MAX];

#define UNVISITED  0

int dfs_num[MAX], dfs_low[MAX], parents[MAX], articulations[MAX];
int num_counter, root_children;

void dfs(int u)
{
    dfs_low[u] = dfs_num[u] = ++num_counter;

    for (auto it: adj[u])
    {
        int v = it;

        if (dfs_num[v] == UNVISITED)
        {
            parents[v] = u;

            if (u == 1) ++root_children;

            dfs(v);

            if (dfs_low[v] >= dfs_num[u])
                articulations[u] = 1;

            dfs_low[u] = min(dfs_low[u], dfs_low[v]);
        } else if (v != parents[u])
            dfs_low[u] = min(dfs_low[u], dfs_num[v]);
    }
}


int main() {

	int n, m;
	int v,w,p;
	while(scanf("%d %d", &n, &m) != EOF) {
		memset(adj,0, sizeof adj);
        memset(dfs_num, UNVISITED, sizeof dfs_num);
        memset(dfs_low, UNVISITED, sizeof dfs_low);
        memset(parents, 0, sizeof parents);
        memset(articulations, 0, sizeof articulations);

		while(m--) {
			scanf("%d %d", &v, &w);
			adj[v].push_back(w);
			adj[w].push_back(v);
		}

        num_counter = root_children = 0;

        dfs(1);

        articulations[1] = (root_children > 1 ? 1 : 0);

        int points = 0;

        for (int i = 1; i <= n; ++i) {
			for(auto it : adj[i]){
				if(dfs_num[i] < dfs_low[it]) {
					points++;
				}
			}
		}
        printf("%d\n", points);

	}


	return 0;
}

