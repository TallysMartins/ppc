/*  Problema das baleias
	algoritmo que detecta pontos de articulacao
    conta se ha ponto de articulaçao entao nao da pra dividir as baleias */
#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;

#define MAX 1001
#define MAXL 4096

#define UNVISITED  0

int adj[MAX][MAX];
int num_counter;
char line[MAXL];
bool bazinga;
int visited[MAX];

void dfs(int u, int n) {
    ++num_counter;
	visited[u] = 1;

    for (int i = 1; i <= n; ++i) {
		if(adj[u][i]) {
			visited[i] = 1;
		}
	}
}

int main()
{
    while (fgets(line, MAXL, stdin))
    {
        int N = atoi(line);
		int nu = N;

		int k = 1;
		int i = 1;
		int v;

        while (N--)
        {
            fgets(line, MAXL, stdin);
            istringstream is(line);

			i = 1;
            while (is >> v)
            {
                adj[k][i] = v;
				++i;
            }
			++k;
        }

        memset(visited, 0, sizeof visited);
        num_counter = 1;

		bazinga = true;
        for (int i = 1; i <= nu; ++i) {
			if(num_counter > 2) {
				bazinga = false;
				break;
			}
			if(not visited[i]) {
				dfs(i,nu);
			}
		}

		if(bazinga)
			cout << "Bazinga!" << endl;
		else
			cout << "Fail!" << endl;
    }

    return 0;
}
