#include <iostream>
#include <cstring>
#include <queue>
#include <cstdio>
#include <algorithm>
#include <cstdio>
#include <map>
#include <cstdlib>
#include <unistd.h>

using namespace std;

#define ii pair<int,int>
typedef struct g{
	pair <int, int>a;
	pair <int, int>b;
	pair <int, int>c;
	int deepth=0;
	char grid[11][11];
} g;

ii a,b,c;//posições dos robos
ii p1,p2,p3;// posicoes dos portais X
char grid[11][11];
int vis[102][102][102];// vetor de posições visitadas, responde a pergunta: ja teve um grid com essa configuracao dos robos?
int order;//tamanho do grid
int cont;//jogadas usadas para colocar os robos na posicao

bool operator!=(const g& lhs, const g& rhs)
{
	lhs.a != rhs.a || lhs.b != rhs.b || lhs.c != rhs.c;
}

void visit(g &item) {
	int sq_a = (item.a.first-1)*order+item.a.second;
	int sq_b = (item.b.first-1)*order+item.b.second;
	int sq_c = (item.c.first-1)*order+item.c.second;
	vector<int> a = { sq_a, sq_b, sq_c};
	sort(a.begin(), a.end()); // garante que o robo A,B,C na posicao 123 é a mesma coisa que robo B,C,A na posicao 123
	vis[a[0]][a[1]][a[2]]= 1;
}


int visited(g &item) {
	int sq_a = (item.a.first-1)*order+item.a.second;
	int sq_b = (item.b.first-1)*order+item.b.second;
	int sq_c = (item.c.first-1)*order+item.c.second;
	vector<int> a = { sq_a, sq_b, sq_c};
	sort(a.begin(), a.end()); // guarda a configuracao, por exemplo A,B,C na posicao 1,2,3 do grid ja foi visitado
	return (vis[a[0]][a[1]][a[2]]== 1);
}

void make_move(char id, ii &cord, char gr[11][11], int x,int y) {
	int xf = cord.first +x;//posicao x final do robo a ser verificada
	int yf = cord.second +y;//posicao y final do robo a ser verificada
	if(gr[xf][yf] == '0') return; //se o proximo campo é 0 é pq vai pra fora do grid, retorna
	if(gr[xf][yf] == '#') return; // se for # ja retorna pois nao eh possivel 
	if(gr[xf][yf] == '.' || gr[xf][yf] == 'X') { //se for . ou X eh possivel entao atualiza a posicao
		cord.first = xf;
		cord.second = yf;
	}
	if(gr[xf][yf] == 'R') { // se tem um robo a minha frente
		if(gr[xf+x][yf+y] == '0') return;// na frente do robo que esta a minha frente tem um 0?
		if(gr[xf+x][yf+y] == '#') return;// na frente do robo que esta a minha frente tem um #?
		if(gr[xf+x][yf+y] == '.' || gr[xf+x][yf+y] == 'X') { // se na frente do robo que ta a minha frente for valido, MOVE
			cord.first = xf;
			cord.second = yf;
		}else if(gr[xf+x][yf+y] == 'R') {//se na frente do robo a minha frente tem outro robo
			if(gr[xf+x+x][yf+y+y] == '0') return;//se na frente dele tambem for 0 aborta
			if(gr[xf+x+x][yf+y+y] == '#') return;// aborta tambem se for $
			if(gr[xf+x+x][yf+y+y] == '.' || gr[xf+x+x][yf+y+y] == 'X') { // se for posicao valida MOVE
				cord.first = xf;
				cord.second = yf;
			}
		}
	}
}

void fix_grid(g &old_item, g &new_item) {
	new_item.grid[p1.first][p1.second] = 'X';
	new_item.grid[p2.first][p2.second] = 'X';
	new_item.grid[p3.first][p3.second] = 'X';
	new_item.grid[old_item.a.first][old_item.a.second] = '.';
	new_item.grid[old_item.b.first][old_item.b.second] = '.';
	new_item.grid[old_item.c.first][old_item.c.second] = '.';
	new_item.grid[new_item.a.first][new_item.a.second] = 'R';
	new_item.grid[new_item.b.first][new_item.b.second] = 'R';
	new_item.grid[new_item.c.first][new_item.c.second] = 'R';
}

void move(char d, queue<g> &q, g item) {
	switch(d){
		case 'D': {
					g aux = item;
					make_move('A',item.a,item.grid, 1, 0);
					make_move('B',item.b,item.grid, 1, 0);
					make_move('C',item.c,item.grid, 1, 0);
					if(aux!=item && !visited(item)) {
						fix_grid(aux, item);
						item.deepth++;
						q.push(item);
					}
					break;
				  }


		case 'R': {
					g aux = item;
					make_move('A',item.a,item.grid, 0, 1);
					make_move('B',item.b,item.grid, 0, 1);
					make_move('C',item.c,item.grid,0, 1);
					if(aux!=item && !visited(item)) {
						fix_grid(aux, item);
						item.deepth++;
						q.push(item);
					}
					break;
				  }

		case 'U': {
					g aux = item;
					make_move('A',item.a,item.grid, -1, 0);
					make_move('B',item.b,item.grid, -1, 0);
					make_move('C',item.c,item.grid, -1, 0);
					if(aux!=item && !visited(item)) {
						fix_grid(aux, item);
						item.deepth++;
						q.push(item);
					}
					break;
				  }

		case 'L': {
					g aux = item;
					make_move('A',item.a,item.grid, 0, -1);
					make_move('B',item.b,item.grid, 0, -1);
					make_move('C',item.c,item.grid, 0, -1);
					if(aux!=item && !visited(item)) {
						fix_grid(aux, item);
						item.deepth++;
						q.push(item);
					}
					break;
				  }
	}

}

int arrived(g &item) {
	bool arr = false;
	if(item.grid[p1.first][p1.second] == 'R' && item.grid[p2.first][p2.second] == 'R' && item.grid[p3.first][p3.second] == 'R') {
		arr = true;
	}
	return arr;
}

void bfs(g rob){
	queue<g> q;
	q.push(rob);
	while(!q.empty()) {
		g item = q.front(); q.pop();
		if(arrived(item)){
			cont = item.deepth;
			return;
		}
		visit(item);
		move('D', q, item);
		move('R', q, item);
		move('U', q, item);
		move('L', q, item);
	}

}

int main(){
	int T;
	cin >> T;
	char tmp;

	for(int j=1; j<=T; ++j) {
		memset(grid, '0', sizeof(grid));
		memset(vis, 0, sizeof(vis));
		cont = 0;
		ii x[3];
		int n = 0;
		order = 0;
		cin >> order;
		for(int i=1; i<=order; ++i) {
			for(int k=1; k<=order; ++k) {
				cin >> grid[i][k];
				if(grid[i][k] == 'X' || grid[i][k] == 'x') {
					grid[i][k] = 'X';
					x[n] = ii(i,k);
					++n;
				}
				if(grid[i][k] == 'A'){
					a = ii(i,k);
					grid[i][k] = 'R';
				}
				if(grid[i][k] == 'B') {
					b = ii(i,k);
					grid[i][k] = 'R';
				}
				if(grid[i][k] == 'C'){
					c = ii(i,k);
					grid[i][k] = 'R';
				}
			}
			scanf("%c", &tmp);
		}

		p1 = x[0]; p2 = x[1]; p3 = x[2];
		g group;
		group.a = a;
		group.b = b;
		group.c = c;
		group.deepth = 0;
		memcpy(group.grid, grid, sizeof(grid));
		bfs(group);
		if (cont == 0) {
			cout << "Case " << j << ": " <<  "trapped" << endl;
		}else {
			cout << "Case " << j << ": " << cont << endl;
		}

	}

	return 0;
}
