#include <iostream>
#include <cstring>
#include <queue>
#include <stack>

#define MAX 2005

using namespace std;
using vi=vector<int>;
using si=stack<int>;

vi adj[MAX];


vi SCC(int u, int& sccs, int& dfs_counter, si& S, vi& dfs_num, vi& dfs_low, vi& visited) {
    dfs_low[u] = dfs_num[u] = dfs_counter++;
    S.push(u);
    visited[u] = 1;

    for (auto v : adj[u])
    {
        if (dfs_num[v] == 0)
            SCC(v, sccs, dfs_counter, S, dfs_num, dfs_low, visited);

        if (visited[v])
            dfs_low[u] = min(dfs_low[u], dfs_low[v]);
    }

    vi us;

    if (dfs_low[u] == dfs_num[u])
    {
        ++sccs;
		int v;

        do {
            v = S.top();
            S.pop();
            visited[v] = 0;
			dfs_counter++;

            us.push_back(v);
        } while (v != u);
    }

    return us;
}

int tarjan(int n)
{
    vi dfs_num(n + 1, 0);
    vi dfs_low(n + 1, 0);
    vi visited(n + 1, 0);
    int sccs = 0, dfs_counter = 0;
    si S;

	for(int i=1; i<=n; ++i) {
		if(not dfs_num[i])
			SCC(i, sccs, dfs_counter, S, dfs_num, dfs_low, visited);
	}
	int	res = (sccs > 1 ? 0 : 1);
    return res;
}

int main() {

	int n, m;
	int v,w,p;
	while(scanf("%d %d", &n, &m), n | m) {
		memset(adj,0, sizeof adj);

		while(m--) {
			scanf("%d %d %d", &v, &w, &p);
				adj[v].push_back(w);
			if(p&2)
				adj[w].push_back(v);
		}

		cout << tarjan(n) << endl;

	}


	return 0;
}
