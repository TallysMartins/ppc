
#include <iostream>
#include <queue>
#include <cstring>
#include <vector>
#include <sstream>
#include <map>
#include <cctype>
#include <bitset>
#define MAX 110
#define INF 2000000
#define MAXL 4096

using namespace std;
using ii=pair<int,int>;
using ii2=pair<ii,bitset<7> >;

unsigned int dist[MAX][MAX][130];
char matrix[MAX][MAX];
char line[MAXL];
bool cheguei;
bitset<7>glorus;
map<char,int>chaves;


void check_adj(queue<ii2> &q, ii2 &item, int a, int b) {
	if(matrix[a][b] == '0' || matrix[a][b] == '#') {
		return;
	}
	int ux = item.first.first;
	int uy = item.first.second;
	char tmp = matrix[a][b];
	bool can_pass = true;
	bitset<7> keys =item.second;
	auto old_pos = keys.to_ulong();

	if(islower((int) tmp)) {
		int pos = chaves[tmp];
		keys.set(pos,1);
	}else if(isupper((int) tmp)) {
		int pos = chaves[tolower(tmp)];
		if(not keys.test(pos)) {
			can_pass = false;
		}
	}
	auto new_pos = keys.to_ulong();

	if(can_pass and (dist[a][b][new_pos] > dist[ux][uy][old_pos] +1)) {
		dist[a][b][new_pos] = dist[ux][uy][old_pos] +1;
		q.push(ii2(ii(a,b),keys));
	}
}

void dijkstra(int arya[2], int saida[2], int x, int y){
	memset(dist, -1, sizeof dist);
	dist[arya[0]][arya[1]][0] = 0;
	bitset<7>xd;
	queue<ii2>pq;
	pq.push(ii2(ii(arya[0],arya[1]),xd));

	int ux, uy, w;
	while(not pq.empty()) {
		auto item = pq.front(); pq.pop();
		ux = item.first.first;
		uy = item.first.second;
		if(ux == saida[0] and uy == saida[1]) {
			int i =0;
			glorus = item.second;
			cheguei = true;
			break;
		}
		check_adj(pq,item,ux,uy+1);
		check_adj(pq,item,ux-1,uy);
		check_adj(pq,item,ux,uy-1);
		check_adj(pq,item,ux+1,uy);
	}
}

int main() {
	chaves['a'] = 6; chaves['b'] = 5; chaves['c'] = 4;	chaves['d'] = 3; chaves['e'] = 2; chaves['f'] = 1; chaves['g'] = 0;
	memset(matrix,48,sizeof matrix);
	int arya[2];
	int saida[2];
	int i=1;
	int j=1;
	while(fgets(line, MAXL, stdin)) {
		istringstream is(line);
		j=1;
		while(is >> matrix[i][j]) {
			if(matrix[i][j] == '@') {
				arya[0] = i;
				arya[1] = j;
			}else if(matrix[i][j] == '*') {
				saida[0] = i;
				saida[1] = j;
			}
			++j;
		}
		++i;
	}

	cheguei = false;
	dijkstra(arya, saida, i, j);

	if( not cheguei) {
		printf("--\n");
	}else {
		printf("%d\n",dist[saida[0]][saida[1]][glorus.to_ulong()]);
	}
	return 0;
}
