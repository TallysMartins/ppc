#include <iostream>
#include <cstring>
#include <sstream>
#include <vector>

using namespace std;

#define MAX 1001
#define MAXL 4096
#define UNVISITED  0

int cores[MAX];
bool bicorolable;
vector<int>graph[MAX];
char line[MAXL];
int visited[MAX];

int cor_inversa(int color){
	return	1 - color;// se chega 0 -> 1-0 =  1;
   			  //se chega 1 -> 1 -1 = 0
}

void dfs(int v, int color){
	if(not bicorolable)
		return;
	cores[v] = color;
	for(auto it : graph[v]) {
		if(cores[it] == -1) {
			dfs(it,cor_inversa(color));
		}else if(cores[it] == color) {
			bicorolable = false;
			return;
		}
	}
}


int main() {
    while (fgets(line, MAXL, stdin)) {
        int N = atoi(line);
		int k = 1;
		int i = 1;
		int v;

		memset(cores, -1, sizeof(cores));
		memset(graph,0,sizeof(graph));

        while (N--) {
            fgets(line, MAXL, stdin);
            istringstream is(line);

			i = 1;
            while (is >> v) {
				if(k!=i and v == 0) {
					graph[k].push_back(i);
					/* cout << "baleia "<<k << " nao fala com " << i << endl; */
				}
				i++;
            }
			++k;
        }

		bicorolable = true;
		dfs(1,0);
		if(bicorolable)
			cout << "Bazinga!" << endl;
		else
			cout << "Fail!" << endl;
    }

    return 0;
}
