#include <iostream>
#include <cstring>
#include <queue>
#include <cstdio>
#include <stack>

using namespace std;

#define MAX 100
#define VISITED 1
#define UNVISITED 0
int visited[MAX];
vector<int> adj[110];
int c;
void dfs(int unvisited_element){
	visited[unvisited_element] = 1;
	for(auto it : adj[unvisited_element]) {
		if(visited[it] == 0) {
			dfs(it);
		}
	}
}

int main(){

	int T = 0;

	scanf("%d", &T);

	for(int i=1; i <= T; ++i) {
		int n, m, x,y;
		int mark = 1;
		c = -1;
		scanf("%d %d", &n, &m);
		memset(adj,0, sizeof(adj));
		memset(visited,0,sizeof(visited));
		for(int k=0; k<m; ++k) {
			scanf("%d", &x);
			scanf("%d", &y);
			adj[x].push_back(y);
			adj[y].push_back(x);
		}

		for(int j=1; j<=n; ++j) {
			if(visited[j] == 0) {
				mark = j;
				dfs(j);
				c++;
			}
		}

		if(c == 0) {
			cout << "Caso #" << i << ": a promessa foi cumprida" << endl;
		}else {
			cout << "Caso #" << i << ": ainda falta(m) "<< c << " estrada(s)"<< endl;
		}

	}

  return 0;
}
