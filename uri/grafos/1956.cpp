/* algoritmo de kruskall para resolver o problema do subgrafo conectado */
/* de menor custo */
/* responde a pergunta: quais arestas eu removo e mantenho o grafo conectado com */
/* as arestas de menor custo? */
#include <iostream>
#include <cstdio>
#include <queue>
#include <sstream>
#define MAXL 4096
char line[MAXL];
using namespace std;
using ii = pair<long long int,long long int>;
using edge = pair<long long int , ii>;

class UFDS {
private:
  vector<int> p, rank, setSizes;// p = vector que guarda os pais da galera, 
  int numSets;	// rank guarda o rank de cada vertice

public:
  UFDS(int N) {
    numSets = N;
    rank.assign(N, 0);
    p.assign(N, 0);
    for (int i = 0; i < N; i++)
      p[i] = i;
    setSizes.assign(N, 1);
  }
  int findSet(int i) {
    return (p[i] == i) ? i : p[i] = findSet(p[i]);
  }
  bool isSameSet(int i, int j) {
    return findSet(i) == findSet(j);
  }
  void unionSet(int i, int j) {
    if (!isSameSet(i, j)) {
      int x = findSet(i), y = findSet(j);

      if (rank[x] > rank[y]) {
        setSizes[findSet(x)] += setSizes[findSet(y)];
        p[y] = x;
      } else {
        setSizes[findSet(y)] += setSizes[findSet(x)];
        p[x] = y;
        if (rank[x] == rank[y])
          rank[y]++;
      }
      numSets--;
    }
  }
  int setSize(int i) {
    return setSizes[findSet(i)];
  }
  int numDisjointSets() {
    return numSets;
  }
};

int kruskal(int V, priority_queue<edge> &q) {
	UFDS ufds(V+1);
	long long int D = 0;
	while(not q.empty()) {
		auto item = q.top();
		q.pop();
		long long int w = -item.first;
		long long int u  = item.second.first;
		long long int v = item.second.second;

		if(not ufds.isSameSet(u,v)) {
			ufds.unionSet(u,v);
			D += w;
		}
	}
	cout << ufds.numDisjointSets() -1  << " " <<  D  << endl;;

	return D;

}
int main() {

	int  n;
	fgets(line, MAXL, stdin);
	n = atoi(line);
	priority_queue<edge> pq;
	for(int i=1; i<n; ++i) {
		long long int x,z;
		fgets(line, MAXL, stdin);
		istringstream is(line);
		is >> x;
		while(is >> x) {
			is >> z;
			pq.push(edge(-z, ii(i,x)));
		}
	}
	kruskal(n, pq);

	return 0;
}
