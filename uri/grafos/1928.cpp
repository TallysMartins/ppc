#include <iostream>
#include <cstring>
#include <queue>
#include <cstdio>
#include <unistd.h>

using namespace std;

#define MAX 100002
vector<int> adj[MAX];
int values[MAX];
vector<int>pos[MAX];
unsigned int visited[MAX];
int deepth[MAX];
int parent[MAX];
int result;
int root = 1;

void dfs(int card_from){
	queue<int> q;
	q.push(card_from);
	while(not q.empty()) {
		int item = q.front();q.pop();
		/* cout << item << endl; */
		/* sleep(2); */
		visited[item] = 1;
		for(auto it : adj[item]) {
			if(not visited[it]) {
				parent[it] = item;
				deepth[it] = deepth[item]+1;
				q.push(it);
			}
		}
	}
}

int path_calc(int n) {
	int a = pos[n].at(0);
	int b = pos[n].at(1);
	if(a == root || b == root)
		return max(deepth[a], deepth[b]);

	int cont = 0;
	int	auxa = a;
	int auxb = b;
	while(a!=b) {
		cout << "das";
		if(deepth[a] < deepth[b])
			b = parent[b];
		else
			a = parent[a];

	}
	int dist = deepth[auxb] - deepth[b] + deepth[auxa] - deepth[a];
	return dist;

}
int main(){
	ios_base::sync_with_stdio(false);

	int T = 0;
	int a;
	int b;
	int i;

	scanf("%d", &T);
	result = 0;

	for(i=1; i <=T; ++i) {
		scanf("%d", &values[i]);
		pos[values[i]].push_back(i);
	}

	for(i =1; i < T; ++i) {
		scanf("%d %d", &a, &b);
		if(not parent[b]) {
			parent[b] = a;
			deepth[b] = deepth[a]+1;
		}
	}
	/* dfs(root); */
	deepth[root] = 0;
	parent[root] = 0;
	for(i =1; i <= T/2; ++i) {
		result+= path_calc(i);

	}

	cout << result << endl;

  return 0;
}
