#include <iostream>
#include <map>
#include <queue>
#include <set>

using namespace std;
using ii = pair<int,int>;
map <int, list<int> > adj;

int bfs(int s, int ttl) {
	set<int> reachable;
	queue<ii>q;

	q.push(ii(s,ttl));
	reachable.insert(s);

	while(not q.empty()) {
		auto p = q.front();
		q.pop();

		int u = p.first;
		int t = p.second;

		if(t==0)
			continue;

		for(auto v: adj[u]) {
			if(reachable.count(v) == 0) {
				reachable.insert(v);
				q.push(ii(v, t-1));
			}
		}
	}
	return adj.size() - reachable.size();
}
int main() {
	int NC, testcase = 0;

	while(scanf("%d", &NC), NC){
		adj.clear();

		while(NC--) {
			int u,v;
			scanf("%d %d", &u, &v);
			adj[u].push_back(v);
			adj[v].push_back(u);
		}
		int s, ttl;

		while(scanf("%d %d", &s, &ttla), s, ttl);
	}


}
