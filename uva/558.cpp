//Wormholes
//Tem ciclo negativo?? 
//Grafo é direcionado? sim
//Algoritmo de bellman-ford

#include <iostream>
#include <list>

using namespace std;

using ii = pair<int,int>;

#define MAX 1010

list<ii> adj[MAX];

bool negative_cycle () {
	return true;
}

int main() {
	int T;
	cin >> T;

	while(T--) {
		int n,m;
		cin >> n >> m;
		while(m--) {
			int u, v, t;
			cin >> u >> v >> t;
			adj[u].push_back(ii(v,t));
		}
	}

	negative_cycle ? cout << "possible " << endl : cout << "not poss" << endl;


	return 0;
}
