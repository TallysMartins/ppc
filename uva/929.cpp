//929 - Number Maze

#include <iostream>
#include <queue>
#include <cstring>

using namespace std;
#define ii pair<int,int>
#define MAX 1000

int grid[MAX][MAX];
unsigned dist[MAX][MAX];

void djisktra(int s, int d) {
	memset(dist, -1, sizeof(dist));
	priority_queue<ii>pq;
	q.push(ii(d,s));

	while(not pq.empty()) {
		auto item = pq.top(); pq.pop();

	}
}

int main() {
	int mazes;
	cin >> mazes;
	int n,m;

	for(int i=0; i<mazes; ++i) {
		memset(grid,-1,sizeof(grid));
		cin >> n;
		cin >> m;

		for(int j=1; j<=n; ++j) {
			for(int k=1; k <=m; ++k ) {
				cin >> grid[j][k];
			}
		}
		cout << djisktra(n,m) + grid[1][1] << endl;

	}

	return 0;
}

